import 'package:cook_book_exercises/routes.dart';
import 'package:flutter/material.dart';
import 'screen/screen.dart';
void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      onGenerateRoute: (settings) => Routes.generateRoute(settings),
      home: const LoginScreen(),
    );
  }
}



