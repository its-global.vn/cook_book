import 'package:cook_book_exercises/screen/screen.dart';
import 'package:flutter/material.dart';

class Routes {
  Routes._();

  static const String startedScreen = "/startedScreen";
  static const String homePageScreen = "/homePageScreen";
  static const String physicsSimulationScreen = "/physicsSimulationScreen";
  static const String animateContainerScreen ="/animateContainerScreen";
  static const String listNetworkImagesScreen = "/listNetworkImagesScreen";
  static const String floatingAppBarScreen = "/floatingAppBarScreen";
  static const String loginScreen = "/loginScreen";
  static const String detailImageScreen = "/detailImageScreen";
  //init screen
  // static String initScreen() => Routes.startedScreen;

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case startedScreen:
        return _GeneratePageRoute(
            widget: const StartedScreen(), routeName: settings.name);
      case homePageScreen:
        return _GeneratePageRoute(
            widget: const HomePageScreen(), routeName: settings.name);
      case physicsSimulationScreen:
        return _GeneratePageRoute(
            widget: const PhysicsSimulationScreen(), routeName: settings.name);
      case animateContainerScreen:
        return _GeneratePageRoute(
            widget: const AnimateContainerScreen(), routeName: settings.name);
      case listNetworkImagesScreen:
        return _GeneratePageRoute(
            widget: const ListNetworkImagesScreen(), routeName: settings.name);
      case floatingAppBarScreen:
        return _GeneratePageRoute(
            widget: const FloatingAppBarScreen(), routeName: settings.name);
      case detailImageScreen:
        return _GeneratePageRoute(
            widget: const DetailImageScreen(), routeName: settings.name,);
      default:
        return _GeneratePageRoute(
            widget: const LoginScreen(), routeName: settings.name);
    }
  }
}

class _GeneratePageRoute extends PageRouteBuilder {
  final Widget? widget;
  final String? routeName;
  _GeneratePageRoute({this.widget, this.routeName})
      : super(
          settings: RouteSettings(name: routeName),
          pageBuilder: (BuildContext context, Animation<double> firstAnimation,
                  Animation<double> secondaryAnimation) =>
              widget!,
          transitionDuration: const Duration(milliseconds: 200),
          transitionsBuilder: (BuildContext context,
                  Animation<double> firstAnimation,
                  Animation<double> secondaryAnimation,
                  Widget child) =>
              SlideTransition(
                textDirection: TextDirection.ltr,
                position: Tween<Offset>(
                  begin: const Offset(1.0, 0.0),
                  end: Offset.zero,
                ).animate(firstAnimation),
                child: child,
              ),
        );
}
