import 'dart:math';

import 'package:cook_book_exercises/widgets/custom_text.dart';
import 'package:flutter/material.dart';
class AnimateContainerScreen extends StatefulWidget {
  const AnimateContainerScreen({Key? key}) : super(key: key);

  @override
  State<AnimateContainerScreen> createState() => _AnimateContainerScreenState();
}

class _AnimateContainerScreenState extends State<AnimateContainerScreen> {
  bool _visible = true;
  double _width = 50;
  double _height= 50;
  Color _color = Colors.black;
  BorderRadius _borderRadius = BorderRadius.circular(8.0);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const CustomText(tittle: "Animate Container Screen"),
      ),
      body: Center(
        child: AnimatedOpacity(
          opacity: _visible ? 1.0 : 0.0,
          duration: const Duration(milliseconds: 500),
          child: AnimatedContainer(
            height: _height,
            width: _width,
            decoration: BoxDecoration(
              color: _color,
              borderRadius: _borderRadius,
            ),
            duration: const Duration(seconds: 1),
            curve: Curves.fastOutSlowIn,
          ),
        ),
      ),
      floatingActionButton: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: FloatingActionButton(
              onPressed: (){
                setState(() {
                  final random = Random();
                  _height =random.nextInt(300).toDouble();
                  _width=random.nextInt(300).toDouble();
                  _color=Color.fromRGBO(
                    random.nextInt(256),
                    random.nextInt(256),
                    random.nextInt(256),
                    1,);
                  _borderRadius =
                      BorderRadius.circular(random.nextInt(100).toDouble());
                });
              },
              child: const Icon(Icons.play_arrow),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: FloatingActionButton(
                onPressed: (){
                  setState(() {
                    _visible = !_visible;
                  });
                },
              tooltip: 'Toggle Opacity',
              child: const Icon(Icons.flip_outlined),
            ),
          )
        ],
      ),
    );
  }
}
