import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
class DetailImageScreen extends StatefulWidget {
  const DetailImageScreen({Key? key}) : super(key: key);

  @override
  State<DetailImageScreen> createState() => _DetailImageScreenState();
}

class _DetailImageScreenState extends State<DetailImageScreen> {
  @override
  Widget build(BuildContext context) {
    final index = ModalRoute.of(context)!.settings.arguments;
    return Hero(
      tag: "$index",
      child: Scaffold(
          body: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Center(
              child: CachedNetworkImage(
                placeholder: (context, url) =>
                const CircularProgressIndicator(),
                imageUrl: 'https://picsum.photos/250?image=$index',
              ),
            ),
          ),
        ),
    );
  }
}
