import 'package:cook_book_exercises/widgets/widgets.dart';
import 'package:flutter/material.dart';
class FloatingAppBarScreen extends StatefulWidget {
  const FloatingAppBarScreen({Key? key}) : super(key: key);

  @override
  State<FloatingAppBarScreen> createState() => _FloatingAppBarScreenState();
}

class _FloatingAppBarScreenState extends State<FloatingAppBarScreen> {
  final items = List<String>.generate(20, (i) => 'Item $i');
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: CustomScrollView(
          slivers: [
            const SliverAppBar(
              backgroundColor: Colors.green,
            ),
            SliverList(
                delegate:SliverChildBuilderDelegate(
                        (context, index) {
                          final item = items[index];
                          return Dismissible(
                              key: UniqueKey(),
                              onDismissed:(direction) {
                                setState(() {
                                  items.removeAt(index);
                                });
                                ScaffoldMessenger.of(context)
                                    .showSnackBar(SnackBar(content: Text('$item dismissed')));
                              },
                              background: Container(color: Colors.red,),
                              child: ListTile(
                                title: CustomText( tittle: item,),
                              )
                          );
                        },
                  childCount: items.length,
                )
            )
          ],
        ),
      ),
    );
  }
}
