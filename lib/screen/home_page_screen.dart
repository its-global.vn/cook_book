import 'package:cook_book_exercises/routes.dart';
import 'package:cook_book_exercises/widgets/widgets.dart';
import 'package:flutter/material.dart';

class HomePageScreen extends StatefulWidget {
  const HomePageScreen({Key? key}) : super(key: key);

  @override
  State<HomePageScreen> createState() => _HomePageScreenState();
}

class _HomePageScreenState extends State<HomePageScreen> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            title: const CustomText(tittle: "Home Page"),
            bottom: const TabBar(
              tabs: [
                CustomText(tittle: 'white'),
                CustomText(tittle: 'green'),
                CustomText(tittle: 'red'),
              ],
            ),
          ),
          body: Stack(
            children: [
              TabBarView(
                children: [
                  Container(
                    color: Colors.white,
                  ),
                  Container(
                    color: Colors.green,
                  ),
                  Container(
                    color: Colors.red,
                  )
                ],
              ),
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CustomTextButton(
                      child: const CustomText(
                        tittle: "Back to Start Screen",
                      ),
                      onPressed: () => Navigator.pop(context),
                    ),
                    CustomTextButton(
                      child: const CustomText(
                        tittle: "To Physics Simulation Screen",
                      ),
                      onPressed: () =>
                          Navigator.pushNamed(context, Routes.physicsSimulationScreen),
                    ),
                    CustomTextButton(
                      child: const CustomText(
                        tittle: "To Animate Container Screen",
                      ),
                      onPressed: () =>
                          Navigator.pushNamed(context, Routes.animateContainerScreen),
                    ),
                    CustomTextButton(
                      child: const CustomText(
                        tittle: "To List Network Images Screen",
                      ),
                      onPressed: () =>
                          Navigator.pushNamed(context, Routes.listNetworkImagesScreen),
                    ),
                    CustomTextButton(
                      child: const Padding(
                        padding: EdgeInsets.all(8.0),
                        child: CustomText(
                          tittle: "To Floating app bar, dismiss items Screen",
                        ),
                      ),
                      onPressed: () =>
                          Navigator.pushNamed(context, Routes.floatingAppBarScreen),
                    ),
                  ],
                ),
              )
            ],
          ),
          drawer: Drawer(
            child: Center(
                child: Column(
              children: [
                 DrawerHeader(
                  decoration: const BoxDecoration(color: Colors.blueAccent),
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: [
                      Container(
                        width: 160.0,
                        color: Colors.red,
                      ),
                      Container(
                        width: 160.0,
                        color: Colors.black,
                      ),
                      Container(
                        width: 160.0,
                        color: Colors.green,
                      ),
                      Container(
                        width: 160.0,
                        color: Colors.yellow,
                      ),
                      Container(
                        width: 160.0,
                        color: Colors.orange,
                      ),
                    ],
                  ),
                ),
                ListTile(
                  title: const CustomText(tittle: 'Item 1'),
                  onTap: () async {
                    Navigator.pop(context);
                    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                      duration: Duration(seconds: 1),
                        content: CustomText(
                      tittle: "You had tap on Item 1",
                      color: Colors.white,
                    )));
                  },
                ),
                ListTile(
                  title: const Row(
                    children: [
                      Icon(Icons.arrow_back),
                      CustomText(tittle: 'Log out'),
                    ],
                  ),
                  onTap: () {
                    Navigator.pushReplacementNamed(context, Routes.loginScreen);
                    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                      duration: Duration(seconds: 1),
                        content: CustomText(
                      tittle: "Log Out",
                      color: Colors.white,
                    )));
                  },
                ),
              ],
            )),
          ),
        ),
      ),
    );
  }
}
