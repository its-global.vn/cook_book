import 'package:cached_network_image/cached_network_image.dart';
import 'package:cook_book_exercises/widgets/widgets.dart';
import 'screen.dart';
import 'package:flutter/material.dart';

class ListNetworkImagesScreen extends StatefulWidget {
  const ListNetworkImagesScreen({Key? key}) : super(key: key);

  @override
  State<ListNetworkImagesScreen> createState() =>
      _ListNetworkImagesScreenState();
}

class _ListNetworkImagesScreenState extends State<ListNetworkImagesScreen> {
  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: "1",
      child: Scaffold(
        appBar: AppBar(
          title: const CustomText(tittle: "List Network Images"),
        ),
        body:OrientationBuilder(
            builder: (context, orientation) {
              return GridView.count(
                crossAxisCount: orientation == Orientation.portrait ? 2 : 3,
                children: List.generate(100, (index) {
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Center(
                      child: InkWell(
                        onTap: () => Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const DetailImageScreen(),
                                settings: RouteSettings(arguments: index))),
                        child: CachedNetworkImage(
                          placeholder: (context, url) =>
                              const CircularProgressIndicator(),
                          imageUrl: 'https://picsum.photos/250?image=$index',
                        ),
                      ),
                    ),
                  );
                }),
              );
            },
          ),
        ),
    );
  }
}
