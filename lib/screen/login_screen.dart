import 'package:cook_book_exercises/routes.dart';
import 'package:cook_book_exercises/widgets/widgets.dart';
import 'package:flutter/material.dart';
class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final TextEditingController controller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    bool isValidate = false;
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: const CustomText(tittle: "Login Screen",),
      ),
      body: Column(
        children: [
           Center(
             child: CustomTextField(
              width: 300,
              initText: "A",
              labelText: "label text",
              hintText: "hin text",
              enableBorder: true,
              validator: (value) {
                if(value == null || value.isEmpty){
                  return "Invalid";
                }
                return null;
              },
               onValidate: () => isValidate= true,
               onInvalid: () => ScaffoldMessenger.of(context).showSnackBar( const SnackBar(
                 content: CustomText(tittle: 'invalid', size: 15,color: Colors.white,),
                 duration: Duration(milliseconds: 200),
               )
               ),
          ),
           ),
          CustomTextButton(
              height: 50,
              width: 100,
              child:const CustomText(
                  tittle: 'Login',
              ),
            onPressed: () {
              if(isValidate) {
                Navigator.pushNamed(context, Routes.startedScreen);
                setState(() {
                  controller.text = "";
                });
              }
            },
          )
        ],
      ),
    );
  }
}
