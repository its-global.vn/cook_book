import 'package:cook_book_exercises/widgets/custom_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/physics.dart';

class PhysicsSimulationScreen extends StatefulWidget {
  const PhysicsSimulationScreen({Key? key}) : super(key: key);

  @override
  State<PhysicsSimulationScreen> createState() =>
      _PhysicsSimulationScreenState();
}

class _PhysicsSimulationScreenState extends State<PhysicsSimulationScreen>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;
  late Animation<Alignment> _animation;
  Alignment _dragAlignment = Alignment.center;

  @override
  void initState() {
    super.initState();
    _animationController =
        AnimationController(vsync: this, duration: const Duration(seconds: 1));
    _animationController.addListener(() {
      setState(() {
        _dragAlignment = _animation.value;
      });
    });
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  void _runAnimation(Offset pixelsPerSecond, Size size) {
    _animation = _animationController.drive(
      AlignmentTween(
        begin: _dragAlignment,
        end: Alignment.center,
      ),
    );
    final unitsPerSecondX = pixelsPerSecond.dx / size.width;
    final unitsPerSecondY = pixelsPerSecond.dy / size.height;
    final unitsPerSecond = Offset(unitsPerSecondX, unitsPerSecondY);
    final unitVelocity = unitsPerSecond.distance;
    // _animationController.reset();
    // _animationController.forward();
    const spring = SpringDescription(
      mass: 30,
      stiffness: 1,
      damping: 1,
    );

    final simulation = SpringSimulation(spring, 0, 1, -unitVelocity);

    _animationController.animateWith(simulation);
  }

  @override
  Widget build(BuildContext context) {
    late var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: const CustomText(tittle: "Physics Simulator Screen"),
      ),
      body: GestureDetector(
        onPanDown: (details) {
          _animationController.stop();
        },
        onPanUpdate: (details) {
          setState(() {
            _dragAlignment += Alignment(
              details.delta.dx / (size.width / 2),
              details.delta.dy / (size.height / 2),
            );
          });
        },
        onPanEnd: (details) {
          _runAnimation(details.velocity.pixelsPerSecond,size);
        },
        child: Align(
          alignment: _dragAlignment,
          child: const Card(
              child: FlutterLogo(
            size: 125,
          )),
        ),
      ),
    );
  }
}
