import 'package:cook_book_exercises/routes.dart';
import 'package:flutter/material.dart';

import '../widgets/widgets.dart';
class StartedScreen extends StatefulWidget {
  const StartedScreen({Key? key}) : super(key: key);

  @override
  State<StartedScreen> createState() => _StartedScreenState();
}

class _StartedScreenState extends State<StartedScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: const CustomText(tittle: "Lets Start"),
      ),
      body: Center(
          child: CustomTextButton(
            child: const CustomText(tittle: "Lets Start"),
            onPressed: () => Navigator.pushNamed(context, Routes.homePageScreen),
          )
      ),
    );
  }
}