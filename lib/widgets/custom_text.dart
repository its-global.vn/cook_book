import 'package:flutter/material.dart';

class CustomText extends StatefulWidget {
  final String tittle;

  final double? size;

  final FontWeight? fontWeight;

  final Color? color;

  final FontStyle? fontStyle;

  final String? fonFamily;

  final Color? backgroundColor;

  final TextDecoration? decoration;
  final TextDecorationStyle? decorationStyle;
  const CustomText(
      {Key? key,
      required this.tittle,
      this.size,
      this.fontWeight,
      this.color,
      this.fontStyle,
      this.backgroundColor,
      this.decoration,
      this.decorationStyle, this.fonFamily})
      : super(key: key);

  @override
  State<CustomText> createState() => _CustomTextState();
}

class _CustomTextState extends State<CustomText> {
  @override
  Widget build(BuildContext context) {
    return Text(
      widget.tittle,
      style: TextStyle(
        fontStyle: widget.fontStyle ?? FontStyle.normal,
        fontSize: widget.size ?? 25,
        fontWeight: widget.fontWeight ?? FontWeight.normal,
        fontFamily: widget.fonFamily ?? 'Raleway',
        color: widget.color ?? Colors.black,
        decoration: widget.decoration,
        decorationStyle: widget.decorationStyle,
      ),
    );
  }
}
