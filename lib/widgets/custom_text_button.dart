import 'package:flutter/material.dart';

class CustomTextButton extends StatefulWidget {
  final Widget child;
  final double? height;
  final double? width;
  final double? borderRadius;
  final Color? textColor;
  final Color? backgroundColor;
  final Color? borderColor;
  final GestureTapCallback? onPressed;
  final GestureLongPressCallback? onLongPressed;


  const CustomTextButton(
      {Key? key,
      required this.child,
      this.textColor,
      this.backgroundColor,
      this.borderColor,
      this.onPressed,
      this.onLongPressed,
      this.height,
      this.width, this.borderRadius})
      : super(key: key);

  @override
  State<CustomTextButton> createState() => _CustomTextButtonState();
}

class _CustomTextButtonState extends State<CustomTextButton> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap:  widget.onPressed,
      onLongPress: widget.onLongPressed,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          height: widget.height ?? 80,
          width: widget.width ?? double.infinity,
          decoration: BoxDecoration(
            color: widget.backgroundColor ?? Colors.white,
            borderRadius: BorderRadius.circular(widget.borderRadius ?? 8.0),
            border: Border.all(color: widget.borderColor ?? Colors.black)
          ),
          child: Center(child: widget.child),
        ),
      ),
    );
  }
}
