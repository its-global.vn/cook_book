import 'package:flutter/material.dart';

typedef CustomValidator<T> = String? Function(String? value); // return null if true

class CustomTextField extends StatefulWidget {
  const CustomTextField(
      {Key? key,
      this.validator,
      this.onChanged,
      this.labelText,
      this.hintText,
      this.onTap,
      this.onTapOutside,
      this.autoFocus,
      this.width,
      this.enableBorder,
      this.borderColor,
      this.onValidate, this.initText, this.controller, this.onInvalid,})
      : super(key: key);
  final TextEditingController? controller;
  final CustomValidator? validator;
  final Function(String)? onChanged;
  final Function()? onTap;
  final Function()? onInvalid;
  final Function()? onValidate;
  final Function(PointerDownEvent)? onTapOutside;
  final String? labelText;
  final String? hintText;
  final String? initText;
  final bool? autoFocus;
  final bool? enableBorder;
  final double? width;
  final Color? borderColor;

  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  @override
  Widget build(BuildContext context) {
    widget.initText !=  null ? widget.controller?.text= widget.initText!: null;
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: 60,
        width: widget.width ?? double.infinity,
        decoration: BoxDecoration(
            borderRadius: (widget.enableBorder ?? true)
                ? BorderRadius.circular(8.0)
                : null,
            border: Border.all(color: widget.borderColor ?? Colors.black)),
        child: Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Form(
            child: TextFormField(
              controller: widget.controller,
              validator: widget.validator,
              decoration: InputDecoration(
                border: InputBorder.none,
                labelText: widget.labelText,
                hintText: widget.hintText,
              ),
              onTap: widget.onTap,
              onTapOutside: widget.onTapOutside,
              autofocus: widget.autoFocus ?? true,
              onChanged: (value) {
                if (widget.validator!(value) != null) {
                  widget.onInvalid!();
                }
                else {
                  widget.onValidate!();
                }
              },
            ),
          ),
        ),
      ),
    );
  }
}
